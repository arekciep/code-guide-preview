# Rules:
- Naming: 
    - PascalCase:
        - components
        - classes
    - camelCase:
        - helper functions
        - hooks
    - interfaces start with `I`
    - instead of nesting folders group components by prefixing them with feature name, example:
        - ProfileChangePassword
        - ProfileDeposit
        - ProfileDepositConfirmation
    - hooks names starts with `use` and `capital letter` right after it

- Folders and files:
    - max 3 nested folders within src
    - no default exports
    - index.ts within every folder re-exporting every public module so instead of writing `import { Button } './Button/Button';` we can just write `import { Button } from './Button';`
    - tests within components folder (ext. `.test.ts`)
    - put `assets` in `public` (`assets` are not `source`)

- Code guide:
    - no class components - use functional components
    - no HOC - use hooks (useHooks hehe)
    - do not use services within components - use it via models
    - one component per file
    - bussines logic within models, render logic within components
    - all models properties must be private
        - if you need to read it outside of function write `public get<<PropertyName>>(): <<Type>>` method
        - if you need to change it outside of function write `public set<<PropertyName>>(): <<Type>>` method
    - return references as `readonly`
    - `Props` types within component file right after imports
    - do not destructure props and function parameters
    - `eslint` and `prettier` will do the rest


# Preview
- `services/**/*`
- `models/Event/*`
- `http/*`
