import { Http } from '../../http';
import { IEventService } from '../IEventService';
import { IEventLiveService } from './IEventLiveService';

export class EventLiveServiceRest implements IEventService, IEventLiveService {
    public fetchEvents(): any {
        return new Http().fetch();
    }
}
