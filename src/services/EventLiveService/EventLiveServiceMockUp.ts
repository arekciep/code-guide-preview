import { IEventService } from '../IEventService';
import { IEventLiveService } from './IEventLiveService';

export class EventLiveServiceMockUp implements IEventService, IEventLiveService {
    public fetchEvents(): any {
        return {};
    }
}
