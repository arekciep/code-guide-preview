import { Http } from '../../http';
import { IEventService } from '../IEventService';

export class EventPregameService implements IEventService {
    public fetchEvents(): any {
        return new Http().fetch();
    }
}
